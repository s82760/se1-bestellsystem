package system;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.List;
import java.util.Locale;

import datamodel.Customer;
import datamodel.Order;
import datamodel.OrderItem;

final class OutputProcessor implements Components.OutputProcessor{
	InventoryManager inventoryManager;
	private final OrderProcessor orderProcessor;
	
	private final int printLineWidth = 95;
	
	public OutputProcessor(InventoryManager inventoryManager, OrderProcessor orderProcessor) {
		this.inventoryManager = inventoryManager;
		this.orderProcessor = orderProcessor;
	}
		
	@Override
	public void printOrders(List<Order> orders, boolean printVAT) {
        StringBuffer sbAllOrders = new StringBuffer( "-------------" );
        StringBuffer sbLineItem = new StringBuffer();
        long totalPrice = 0;
        printVAT = true;
        long totalVat = 0;

        /*
         * Insert code to print orders with all order items:
         */
        //List<Order> orders = new ArrayList<Order>( List.of( o5234, o8592, o3563, o6135 ) );

        for (Order o : orders) //Id und Name des Kundens wird in erster Schleife geholt
        {
        	Customer customer = o.getCustomer();
        	String customerName = splitName( customer, customer.getFirstName() + " " + customer.getLastName() );
            String s = "#" + o.getId() + "," + " " + customerName + "'s Bestellung" + ": "; //hier gibts noch Probleme mit dem Namen...
            
            long price = 0; 
            int count = 0;
                    for (OrderItem item : o.getItems()) //holt sich alle Items mit Preis und Beschreibung
                    {
                          price += item.getUnitsOrdered() * item.getArticle().getUnitPrice();
                          s += item.getUnitsOrdered() + "x " + item.getDescription();

                          if (count == o.count() - 1)
                          {
                              break;
                          }

                          s += ", ";
                          count++;
                    }
                    count = 0;

            String fmtPrice = fmtPrice(price, "EUR", 14 );
            totalPrice += price; //finaler Preis wird berechnet
            long vat = orderProcessor.vat(price, 1);
            totalVat += vat;
            sbLineItem = fmtLine(s, fmtPrice, printLineWidth);
            sbAllOrders.append( "\n" );
            sbAllOrders.append( sbLineItem );
        }
        // calculate total price
        String fmtPriceTotal = pad( fmtPrice( totalPrice, "", " EUR" ), 14, true );
        String fmVat = pad( fmtPrice( totalVat, "", " EUR" ), 14, true );

        // append final line with totals
        sbAllOrders.append( "\n" )
            .append( fmtLine( "-------------", "------------- -------------", printLineWidth ) )
            .append( "\n" )
            .append( fmtLine( "Gesamtwert aller Bestellungen:", fmtPriceTotal, printLineWidth ) )
            .append( "\n" )
        	.append( fmtLine( "Im Gesamtbetrag enthaltender Mehrwertsteur(19%) :", fmVat, printLineWidth ) );

        // print sbAllOrders StringBuffer with all output to System.out
        System.out.println( sbAllOrders.toString() );
	}


	private String fmtPrice( long price, String prefix, String postfix ) {
		StringBuffer fmtPriceSB = new StringBuffer();
		if( prefix != null ) {
			fmtPriceSB.append( prefix );
		}

		fmtPriceSB = fmtPrice( fmtPriceSB, price );

		if( postfix != null ) {
			fmtPriceSB.append( postfix );
		}
		return fmtPriceSB.toString();
	}


	private String pad( String str, int width, boolean rightAligned ) {
		String fmtter = ( rightAligned? "%" : "%-" ) + width + "s";
		String padded = String.format( fmtter, str );
		return padded;
	}
	

	private StringBuffer fmtPrice( StringBuffer sb, long price ) {
		if( sb == null ) {
			sb = new StringBuffer();
		}
		double dblPrice = ( (double)price ) / 100.0;			// convert cent to Euro
		DecimalFormat df = new DecimalFormat( "#,##0.00",
			new DecimalFormatSymbols( new Locale( "de" ) ) );	// rounds double to 2 digits

		String fmtPrice = df.format( dblPrice );				// convert result to String in DecimalFormat
		sb.append( fmtPrice );
		return sb;
	}
	
	@Override
	public void printInventory() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public String fmtPrice(long price, String currency) {
		String fmtPrice = pad( fmtPrice( price, "", " " + currency ), 14, true );
		return fmtPrice;
	}

	@Override
	public String fmtPrice(long price, String currency, int width) {
		String fmtPrice = pad( fmtPrice( price, "", " " + currency ), 14, true );
		return fmtPrice;
	}

	@Override
	public StringBuffer fmtLine(String leftStr, String rightStr, int width) {
		StringBuffer sb = new StringBuffer( leftStr );
		int shiftable = 0;		// leading spaces before first digit
		for( int i=1; rightStr.charAt( i ) == ' ' && i < rightStr.length(); i++ ) {
			shiftable++;
		}
		final int tab1 = width - rightStr.length() + 1;	// - ( seperator? 3 : 0 );
		int sbLen = sb.length();
		int excess = sbLen - tab1 + 1;
		int shift2 = excess - Math.max( 0, excess - shiftable );
		if( shift2 > 0 ) {
			rightStr = rightStr.substring( shift2, rightStr.length() );
			excess -= shift2;
		}
		if( excess > 0 ) {
			switch( excess ) {
			case 1:	sb.delete( sbLen - excess, sbLen ); break;
			case 2: sb.delete( sbLen - excess - 2 , sbLen ); sb.append( ".." ); break;
			default: sb.delete( sbLen - excess - 3, sbLen ); sb.append( "..." ); break;
			}
		}
		String strLineItem = String.format( "%-" + ( tab1 - 1 ) + "s%s", sb.toString(), rightStr );
		sb.setLength( 0 );
		sb.append( strLineItem );
		return sb;
	}

	@Override
	public String splitName(Customer customer, String name) {	    
		String fname = customer.getFirstName();
	    String lname = customer.getLastName();
		if(name.contains(",")) { //falls ein Name ein Komma enth�lt, soll folgendes passieren:
	    	if(name.split(",").length>1) { //Name wird beim "," geteilt
	    		fname = name.substring(name.lastIndexOf(",")+1).replace(" ", ""); //alles hinterm Komma wird der Vorname und Leerzeichen werden entfernt
	    		lname = name.substring(0, name.lastIndexOf(',')); //alles vor dem Komma ist der Nachname
	    	}
	    }		
	    else {
			if(name.split(" ").length>1){ //Name wird beim Leerzeichen in mehrere Strings geteilt
				lname = name.substring(name.lastIndexOf(" ")+1); 
				fname = name.substring(0, name.lastIndexOf(' '));	
			}
	    }
		customer.setFirstName(fname);
        customer.setLastName(lname);
		return name;
		
	}

	@Override
	public String singleName(Customer customer) {
		return customer.getFirstName() + customer.getLastName();
	}

}
