package system;

import java.util.function.Consumer;

import datamodel.Order;
import datamodel.OrderItem;

final class OrderProcessor implements Components.OrderProcessor{
	InventoryManager inventoryManager;
	
	/**
	 * 
	 * @param inventoryManager
	 */
	public OrderProcessor(InventoryManager inventoryManager) {
		this.inventoryManager = inventoryManager;
	}
	
	@Override
	public boolean accept(Order order) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean accept(Order order, Consumer<Order> acceptCode, Consumer<Order> rejectCode,
			Consumer<OrderItem> rejectedOrderItemCode) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public long orderValue(Order order) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public long vat(long grossValue) { // rateIndex: 1 as default (19% MwSt)
		return this.vat(grossValue, 1);
	}

	//unterscheidet zwischen verschiedenen Steuersätzen
	@Override
	public long vat(long grossValue, int rateIndex) {
		// rateIndex=1 der MwSt.‐Satz von 19% 
		// und mit rateIndex=2 der MwSt.‐Satz von 7%
		if (rateIndex == 1) {
			grossValue = (grossValue * 19)/119;
		}
		if (rateIndex == 2) {
			grossValue = (grossValue * 7)/107;
		}
		return grossValue;
	}

}
