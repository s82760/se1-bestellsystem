package datamodel;

public class OrderItem {
	private String description;
	private final Article article;
	private int unitsOrdered;
	
	/**
	 * 
	 * @param description
	 * @param article
	 * @param units
	 */
	protected OrderItem(String description, Article article, int units) {
		this.description = description;
		this.article = article;
		this.unitsOrdered = units;
	}
	
	public String getDescription() {
		if(description == null) {
			return "";
		}
		else {
			return description;
		}
		
	}
	
	public void setDescription(String descr) {
		this.description = descr;
	}
	
	public Article getArticle() {
		return article;
	}
	
	public int getUnitsOrdered() {
		if(unitsOrdered < 0) {
			return 0;
		}
		return unitsOrdered;
	}
	
	public void setUnitsOrdered(int number) {
		this.unitsOrdered = number;
	}
}
