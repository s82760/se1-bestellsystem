package datamodel;

public class Article {
	private String id;
	private String description;
	private long unitPrice;
	private int unitsInStore;
	
	/**
	 * 
	 * @param id
	 * @param descr
	 * @param price
	 * @param units
	 */
	protected Article(String id, String descr, long price, int units) {
		this.id = id;
		this.description = descr;
		this.unitPrice = price;
		this.unitsInStore = units;
	}
	
	public String getId() {
		return id;
	}
	
	public String getDescription() {
		if(description == null) {
			return "";
		}
		return description;
	}
	
	public void setDescription(String descr) {
		if(descr == null) {
			this.description = "";
		}
		else {
			this.description = descr;
		}
	}
	
	public void setUnitPrice(long unitprice) {
		if(unitprice < 0 || unitprice == Long.MAX_VALUE) {
			this.unitPrice = 0;
		}
		else {
			this.unitPrice = unitprice;
		}

	}
	
	public long getUnitPrice() {
		if(unitPrice < 0) {
			return 0;
		}
		return unitPrice;
	}
	
	public int getUnitsInStore() {
		if(unitsInStore < 0) {
			return 0;
		}
		return unitsInStore;
	}
	
	public void setUnitsInStore(int number) {
		if(number < 0 || number == Integer.MAX_VALUE) {
			this.unitsInStore = 0;
		}
		else {
			this.unitsInStore = number;		
		}
	}
}
