package datamodel;

public class Customer {
	private final String id;
	private String firstName;
	private String lastName;
	private String contact;
	

	/**
	 * 
	 * @param id
	 * @param name
	 * @param contact
	 */
	protected Customer(String id, String name, String contact){
		this.id = id;
		this.firstName = "";
		this.lastName = name;
		this.contact = contact;
	}
	
	public String getId() {
		return id;
	}
	
	public String getFirstName() {
		if(firstName == null) {
			return firstName = "";
		}
		return firstName;
	}
	
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	
	public String getLastName() {
		if(lastName == null) {
			return lastName = "";
		}
		return lastName;
	}
	
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	public String getContact() {
		if(contact == null) {
			return contact = "";
		}
		return contact;
	}
	
	public void setContact(String contact) {
		this.contact = contact;
	}
	
}
